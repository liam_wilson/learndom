//EXAMINE THE DOCUMENT//

// console.dir(document);
// console.log(document.domain);
// console.log(document.url);
// console.log(document.title);
// document.title = 'skuxx';
// console.log(document.head);
// console.log(document.body);
// console.log(document.all);
// console.log(document.all[10]);
// document.all[10].textContent = 'hello';
// console.log(document.forms);
// console.log(document.links);
// console.log(document.images);

// GETELEMENTBYID
// console.log(document.getElementById('header-title'));
// var headerTitle = document.getElementById('header-title');
// var header = document.getElementById('main-header');
// headerTitle.textContent = 'hello';
// headerTitle.innerText = 'Goodbye';
// console.log(headerTitle.innerText);
////// INNERHTML PUTS IN MIDDLE OF HTML ///////
// headerTitle.innerHTML = '<h3>hello</h3>';
// console.log(headerTitle.innerText);
// header.style.borderBottom = 'solid 3px #000';

// GETELEMENTSBYCLASSNAME
// var items = document.getElementsByClassName('list-group-item');
// console.log(items);
// console.log(items[2]);
// items[1].textContent = 'hello2';
// items[1].style.fontWeight = 'bold';
// items[1].style.backgroundColor = 'green';

// // items.style.backgroundColor = '#f4f4f4';

// for(var i = 0; i < items.length; i++){
//     items[i].style.backgroundColor = '#f3f3f3';
// }

// GETELEMENTSBYTAGNAME
// var li = document.getElementsByTagName('li');
// console.log(li);
// console.log(li[2]);
// li[1].textContent = 'hello2';
// li[1].style.fontWeight = 'bold';
// li[1].style.backgroundColor = 'green';

// // li.style.backgroundColor = '#f4f4f4';

// for(var i = 0; i < li.length; i++){
//     li[i].style.backgroundColor = '#f3f3f3';
// }

// QUERYSELECTOR
// var header = document.querySelector('#main-header');
// header.style.borderBottom = 'solid 3px #ccc';

// var input = document.querySelector('input');
// input.value = 'hello world';

// var submit = document.querySelector('input[type="submit"]');
// submit.value = "send";

// var item = document.querySelector('.list-group-item');
// item.style.color = 'red';

// var lastItem = document.querySelector('.list-group-item:last-child');
// lastItem.style.color = 'orange';

// var secondItem = document.querySelector('.list-group-item:nth-child(2)');
// secondItem.style.color = 'coral';

//QUERYSELECTORALL
// var titles = document.querySelectorAll('.title');

// console.log(titles);
// titles[0].textContent = 'hello';

// var odd = document.querySelectorAll('li:nth-child(odd)');
// var even = document.querySelectorAll('li:nth-child(even)');

// for (var i = 0; i < odd.length; i++){
//     odd[i].style.backgroundColor = '#f4f4f4';
//     even[i].style.backgroundColor = '#121212';
// }



///// TRAVERSING THE DOM /////
// var itemList = document.querySelector('#items');
// parentNode
// console.log(itemList.parentNode);
// itemList.parentNode.style.backgroundColor = '#f3f3f3';
// console.log(itemList.parentNode);

// parentElement
// console.log(itemList.parentElement);
// itemList.parentElement.style.backgroundColor = '#f3f3f3';
// console.log(itemList.parentElement);


// // childNodes  (don't use it is toxic)
// console.log(itemList.childNodes);

// use this to get children
// console.log(itemList.children);
// console.log(itemList.children[1]);
// itemList.children[1].style.backgroundColor = 'yellow';

// FirstChild (don't use cunt)
// console.log(itemList.firstChild);

// firstElementChild
// console.log(itemList.firstElementChild);
// itemList.firstElementChild.textContent = 'heeee';

// lastChild (don't use cunt)
// console.log(itemList.lastChild);

// lastElementChild
// console.log(itemList.lastElementChild);
// itemList.lastElementChild.textContent = 'dd';

// nextSibling (don't use or die)
// console.log(itemList.nextSibling);

// nextElementSibling 
// console.log(itemList.nextElementSibling);

//previousSibling (don't useseee)
// console.log(itemList.previousSibling);
//previousElementSibling
// console.log(itemList.previousElementSibling);



// createElement

// // create a div
// var newDiv = document.createElement('div');

// // Add Class
// newDiv.className = 'hello';

// // add ID
// newDiv.id = 'hello1';

// // add attribute
// newDiv.setAttribute('title', 'Hello Dick');

// // create a text node
// var newDivText = document.createTextNode('helloworld');

// // add text to div
// newDiv.appendChild(newDivText);

// var container = document.querySelector('header .container');
// var h1 = document.querySelector('header h1');

// console.log(newDiv);

// newDiv.style.fontSize = '30px';

// container.insertBefore(newDiv, h1);


////// EVENTS /////


// var button = document.getElementById('button').addEventListener('click', buttonClick);

// function buttonClick(e){
//     //console.log('Button Clicked');
//     // document.getElementById('header-title').textContent = 'changed';
//     // document.querySelector('#main').style.backgroundColor = '#f4f4f4';
//     // console.log(e);

//     // console.log(e.target);
//     // console.log(e.target.id);
//     // console.log(e.target.className);
//     // console.log(e.target.classList);
//     // var output = document.getElementById('output');
//     // output.innerHTML = '<h3>'+e.target+'</h3>';

//     // console.log(e.type);

//     //from the window (browser)
//     // console.log(e.clientX);
//     // console.log(e.clientY);

//     //on the element (button)
//     // console.log(e.offsetY);

//     // check if the altkey on when the button pressed
//     // console.log(e.altKey);
// }


var button = document.getElementById('button');
var box = document.getElementById('box');

// button.addEventListener('click', runEvent);
// button.addEventListener('dblclick', runEvent);
// button.addEventListener('mousedown', runEvent);
// button.addEventListener('mouseup', runEvent);

// box.addEventListener('mouseenter', runEvent);
// box.addEventListener('mouseleave', runEvent);

// box.addEventListener('mouseover', runEvent);
// box.addEventListener('mouseout', runEvent);

box.addEventListener('mousemove', runEvent);

function runEvent(e){
    console.log('EVENT TYPE: '+e.type);

   // output.innerHTML = '<h3>mouse x: '+e.offsetX+' </h3><h3>'+e.offsetY+'</h3>';

   // document.body.style.backgroundColor = "rgb("+e.offsetX+","+e.offsetY+",40)";
}











